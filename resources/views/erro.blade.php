@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <h2>ERROR INESPERADO</h2>
                        Ha ocurrido un error. Contacte con el administrador
                    <div class="row">
                        <div class="col-md-12 row-block">
                            <a href="{{ url('/home') }}" class="btn btn-lg btn-primary btn-block">
                                <strong>Volver al Inicio</strong>
                            </a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection