@extends('layouts.app')

@section('formIncidencia')

@foreach ($datos as $dato)
<div class="container">
<div class="row justify-content-center">
  <div class="col-md-10">
      <h3>Editar Datos</h3>
<form action="/editarIncidencia/{{$dato->numero}}" method="post">
@csrf
        <div class="form-group">
          <label>Aula</label>
          <input value="{{$dato->aula}}" type="text" class="form-control" id="aula" name="aula">
        </div>
        <div class="form-group">
          <label>Ordenador</label>
          <input value="{{$dato->ordenador}}" type="text" name="ordenador" id="ordenador" class="form-control">
        </div>
        <div class="form-group">
          <label>Descripcion</label>
          <input value="{{$dato->descripcion}}" type="text" name="descripcion" id="descripcion" class="form-control">
        </div>
        <div class="card-body d-flex justify-content-between align-items-center">
          <button type="submit" class="btn btn-outline-success btn-lg col-4">Guardar</button>
          <a class="btn btn-outline-danger btn-lg col-4" href="{{ url('home') }}"> Cancelar</a>
        </div>
      </form>
      @if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
</div>
</div>
@endforeach
@endsection
