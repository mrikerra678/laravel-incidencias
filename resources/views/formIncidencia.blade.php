@extends('layouts.app')

@section('formIncidencia')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-10">
    <form action="formIncidencias" method="post">
        @csrf
        <div class="form-group">
            <label>Aula</label>
            <input type="text" name="aula"  class="form-control" id="aula">
        </div>
        <div class="form-group">
            <label>Ordenador</label>
            <input type="text" name="ordenador" class="form-control" id="ordenador">
        </div>
        <div class="form-group">
            <label>Codigo</label>
            <select name="codigo" value="Elija Codigo del Problema" class="form-control">
              <option value="1-No se enciende la CPU">1-No se enciende la CPU</option>
              <option value="2-No se enciende la pantalla">2-No se enciende la pantalla</option>
              <option value="3-No entra en mi sesión">3-No entra en mi sesión</option>
              <option value="4-No navega en Internet">4-No navega en Internet</option>
              <option value="5-No se oye el sonido">5-No se oye el sonido</option>
              <option value="6-No lee el DVD/CD">6-No lee el DVD/CD</option>
              <option value="7-Teclado/Raton roto">7-Teclado/Raton roto</option>
              <option value="8-Otros...">8-Otros...</option>
            </select>
          </div>
        <div class="form-group">
            <label>Descripcion</label>
            <input type="text" name="descripcion" class="form-control" id="descripcion">
        </div>
        <div class="card-body d-flex justify-content-between align-items-center">
            <button type="submit" class="btn btn-outline-success btn-lg col-4">Enviar</button>
            <a class="btn btn-outline-danger btn-lg col-4" href="{{ url('home') }}"> Cancelar</a>
        </div>
    </form>
    @if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
</div>
</div> 

@endsection