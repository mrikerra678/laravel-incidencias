@extends('layouts.app')

@section('homeprofe')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div><a class="btn btn-secondary btn-lg btn-block" href="{{ url('formIncidencias') }}"> Crear Incidencias </a> </div>
        </div>
          
          
          <div class="col-md-12">
              <br>
              <br>
             
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Aula</th>
                  <th scope="col">Ordenador</th>
                  <th scope="col">Profesor</th>
                  <th scope="col">Codigo</th>
                  <th scope="col">Descripcion</th>
                  <th scope="col">Estado</th>
                  <th scope="col">Editar</th>
                  <th scope="col">Eliminar</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($datos as $dato)
                 <tr>
                            <td scope="row" >{{$dato->numero}}</td>
                            <td >{{$dato->aula}}</td> 
                            <td >{{$dato->ordenador}}</td> 
                            <td >{{$dato->profesor_id}}</td> 
                            <td >{{$dato->codigo}}</td> 
                            <td >{{$dato->descripcion}}</td> 
                            <td >{{$dato->estado}}</td>
                            <td><a href="/editarIncidencia/{{$dato->numero}}"><img src="{{URL::asset('../images/Editar.png')}}" width="40px"></a></td>
                            <td><a href="/eliminarIncidencia/{{$dato->numero}}"><img src="{{URL::asset('../images/Cancelar.png')}}" width="40px"></a></td>
                  </tr>
                @endforeach
                </tbody>
            </table>
          </div>               
    </div>
</div>

@endsection
