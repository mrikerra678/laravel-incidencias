<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//RUTAS PARA LOGUEAR EN GOOGLE
Route::get('auth/google', 'Auth\LoginController@redirectToGoogle');
Route::get('callback', 'Auth\LoginController@handleGoogleCallback');

Auth::routes();


//RUTAS PARA LOS ADMINS
Route::group(['middleware' => 'admin'], function () {
    Route::get('homeadmin','HomeController@index2');
    Route::get('formIncidenciasadmin','IncidenciasController@IndexAdmin');
    Route::post('formIncidenciasadmin','IncidenciasController@GuardarIncidenciaAdmin');
    Route::get('eliminarIncidenciadmin/{numero}','IncidenciasController@EliminarIncidenciadmin');
    Route::get('editarIncidenciadmin/{numero}','IncidenciasController@EditarIncidencia2');
    Route::post('editarIncidenciadmin/{numero}','IncidenciasController@CambiarEstado');
});



//RUTA PRINCIPAL

Route::get('/home', 'HomeController@index')->name('home');

//
Route::get('/error',function(){return view('erro');});


//CREAR INCIDENCIAS
Route::get('formIncidencias','IncidenciasController@Index');
Route::post('formIncidencias','IncidenciasController@GuardarCambios');


//EDITAR INCIDENCIAS

Route::group(['prefix'=>'editarIncidencia'],function(){
    Route::get('/{numero}','IncidenciasController@EditarIncidencia');
    Route::post('/{numero}','IncidenciasController@GuardarIncidencia');
});
                                                          
//ELIMINAR INCIDENCIAS
Route::get('eliminarIncidencia/{numero}','IncidenciasController@EliminarIncidencia');


Route::get('/enviarcorreo','EmailController@añadir');
Route::get('/enviarcorreo2','EmailController@Cambiar');
Route::get('/enviarcorreo3','EmailController@Finalizada');
