<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidencias extends Model
{
    protected $table='incidencias';

    public function incidencias(){
        return $this-> hasOne("App/Profesor");
    }
    protected $fillable = ['aula','codigo','descripcion','comentario','ordenador','estado'];
}
