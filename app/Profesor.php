<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Profesor extends Authenticatable
{
    protected $table = 'profesor';
    public function profesor(){
        return $this-> hasOne("App/Incidencias");
    }
    protected $fillable = ['name', 'email', 'google_id','avatar','avatar_original'];
}
