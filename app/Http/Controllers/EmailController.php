<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profesor;
use Mail;
use Auth;
use App\Incidencias;

class EmailController extends Controller
{
    public function añadir(Request $request){
        $subject = "Has creado una incidencia";
        $usuario = Auth::user()->email;
        $email="ostialucia69@gmail.com";
        $for= $usuario;
        Mail::send('emailcrearinc',$request->all(), function($msj) use ($subject,$for){
            $msj->from("ostialucia69@gmail.com","Incidencias");
            $msj->subject($subject);
            $msj->to($for);
        });
        return redirect('/homeadmin');
    }
    public function Cambiar(Request $request){
        $subject = "Incidencia Cambiada";
        $usuario = Auth::user()->email;
        $email="ostialucia69@gmail.com";
        $for= $usuario;
        Mail::send('emailcambiosinc',$request->all(), function($msj) use ($subject,$for){
            $msj->from("ostialucia69@gmail.com","Incidencias");
            $msj->subject($subject);
            $msj->to($for);
        });
        return redirect('/homeadmin');
    }

    public function Finalizada(Request $request){
        $subject = "Incidencia Finalizada";
        $usuario = Auth::user()->email;
        $email="ostialucia69@gmail.com";
        $for= $usuario;
        Mail::send('emailfinalizada',$request->all(), function($msj) use ($subject,$for){
            $msj->from("ostialucia69@gmail.com","Incidencias");
            $msj->subject($subject);
            $msj->to($for);
        });
        return redirect('/homeadmin');
    }
}
