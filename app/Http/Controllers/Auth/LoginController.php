<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\Profesor;
use Auth;
use Expeciton;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
   

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
   
    public function handleGoogleCallback()
    {
        try {
  
            $user = Socialite::driver('google')->stateless()->user();
   
            $finduser = Profesor::where('google_id', $user->id)->first();
   
            if($finduser){
                Auth::logout();
                Auth::login($finduser);
                if($finduser->admin==1){
                    return redirect ("/homeadmin");
                }
                else{
                    return redirect ("/home");
                }
            }
            else if(strpos($user->email, 'plaiaundi.net')){
                $newUser = Profesor::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'avatar' => $user->avatar,
                    'avatar_original' => $user->avatar_original
                ]);
                
                Auth::login($newUser);
                return redirect('/home');
   
            }
            else if(strpos($user->email, 'plaiaundi.com')){
                $newUser = Profesor::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'avatar' => $user->avatar,
                    'avatar_original' => $user->avatar_original
                ]);
                
                Auth::login($newUser);
                return redirect('/home');
   
            }
            else{
                Auth::logout();
                $email=$user->email;
                return view('erro',['email'=>$email]);
            }
  
        } catch (Exception $e) {
            return view('erro');
        }
    }

}