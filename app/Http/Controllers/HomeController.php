<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use IncidenciasController;
use App\Incidencias;
use App\Profesor;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

public function index()
{
    $id=Auth::user()->id;
    $inciusuario = Incidencias::select('numero','profesor_id','ordenador','aula','codigo','descripcion','estado')->where('profesor_id', $id)->get();
    return view('homeprofe',['datos'=>$inciusuario]);
}


public function index2()
{
    $inciusuario = Incidencias::select('numero','profesor_id','ordenador','aula','codigo','descripcion','estado')->get();
    return view('homeadmin',['datos'=>$inciusuario]);
}
}




