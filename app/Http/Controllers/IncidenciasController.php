<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Http\FormRequest ;
use Illuminate\Support\Facades\Validator;
use App\Incidencias;
use App\Profesor;
use Auth;
use DB;

class IncidenciasController extends Controller
{

    //ENVIA AL FORMULARIO DE CREAR INCIDENCIA
   function Index(){
       return view('formIncidencia');
   }
   function Indexadmin(){
    return view('formIncidenciaAdmin');
}

   //VALIDA LOS DATOS Y LOS GUARDA EN LA BBDD Y TENVIA CORREOS USUARIOS NORMALES
   function GuardarCambios(Request $request){
        $validator = Validator::make($request->all(), [
            'aula' => 'required|numeric|digits_between:1,3',
            'descripcion' => 'required|max:255',
            'codigo' => 'required',
            'ordenador' => 'required|regex:/^HZ[0-9]{6}/'
        ]);
    if ($validator->fails()) {
        return redirect('formIncidencias')
                    ->withErrors($validator)
                    ->withInput();
    }
    else{
        $usu = new Incidencias();
        $usu->aula = $request->aula;
        $usu->codigo =$request->codigo;
        $usu->descripcion = $request->descripcion;
        $usu->ordenador = $request->ordenador;
        $usu->profesor_id = Auth::user()->id;
        $usu->save();
    }
        return redirect('/enviarcorreo');
    }


    //VALIDA LOS DATOS Y LOS GUARDA EN LA BBDD Y TENVIA CORREOS USUARIOS ADMIN
    function GuardarIncidenciaAdmin(Request $request){
        $validator = Validator::make($request->all(), [
            'aula' => 'required|numeric|digits_between:1,3',
            'descripcion' => 'max:255|alpha',
            'ordenador' => 'required|regex:/^HZ[0-9]{6}/'
        ]);
    if ($validator->fails()) {
        return redirect('formIncidencias')
                    ->withErrors($validator)
                    ->withInput();
    }
    else{
        $usu = new Incidencias();
        $usu->aula = $request->aula;
        $usu->descripcion = $request->descripcion;
        $usu->ordenador = $request->ordenador;
        $usu->profesor_id = Auth::user()->id;
        $usu->save();
    }
        return redirect('/enviarcorreo');
    }




    //ELIMINAR INCIDENCIAS DE USUARIOS NORMALES

    function EliminarIncidencia($numero){
        $inciborrar = Incidencias::select('numero','profesor_id','ordenador','aula','descripcion','estado')->where('numero', $numero)->where('profesor_id',Auth::user()->id)->get();
        if(empty($inciborrar[0])){
            
            return redirect('/error');
        }
        $inciborrar->delete();
        return redirect('/enviarcorreo3');
    }

    //ELIMINAR INCIDENCIAS DE USUARIOS ADMINISTRADOR
    function EliminarIncidenciadmin($numero){
        $inciborrar = Incidencias::select('numero','profesor_id','ordenador','aula','descripcion','estado')->where('numero', $numero);
        $inciborrar->delete();
        return redirect('/enviarcorreo3');
    }

    
    //SELLECIONA DATOS DEL BBDD Y ENVIA LOS DATOS PARA EDITAR INCIDENCIAS DE USUARIOS 
    function EditarIncidencia($numero){
        $usuario = Auth::user()->id ;
        
        $incieditar = Incidencias::select('numero','profesor_id','ordenador','aula','descripcion','estado')->where('numero', $numero )->where('profesor_id',Auth::user()->id)->get();
        if(empty($incieditar[0])){
            
            return redirect('/error');
        }
        return view('editarIncidencia',['datos'=>$incieditar]);
       
       
                
        
    }

    //SELLECIONA DATOS DEL BBDD Y ENVIA LOS DATOS PARA EDITAR INCIDENCIAS DE ADMIN 
    function EditarIncidencia2($numero){
        $incieditar = Incidencias::select('numero','profesor_id','ordenador','aula','descripcion','comentario','estado')->where('numero', $numero)->get();
        return view('editarIncidenciaAdmin',['datos'=>$incieditar]);
        
    }


    // COGE LOS DATOS DEL FORMULARIO Y LOS GUARDA EN LA BBDD CON USUARIOS NORMALES ENVIA UN CORREO
    function GuardarIncidencia(Request $request,$numero){
        $validator = Validator::make($request->all(), [
            'aula' => 'required|numeric|digits_between:1,3',
            'descripcion' => 'max:255',
            'codigo' => 'required',
            'ordenador' => 'required|regex:/^HZ[0-9]{6}/'
        ]);
    if ($validator->fails()) {
        return redirect('formIncidencias')  
                    ->withErrors($validator)
                    ->withInput();
    }
    else{
        DB::table('incidencias')
        ->where('numero', $numero)  
        ->limit(1) 
        ->update([
                'ordenador' => $request->ordenador,
                'aula' => $request->aula,
                'descripcion' => $request->descripcion,
        ]); 
        return redirect('/enviarcorreo2');
    }
    }

       // COGE LOS DATOS DEL FORMULARIO Y LOS GUARDA EN LA BBDD CON USUARIOS ADMIN Y ENVIA UN CORREO

    function CambiarEstado(Request $request,$numero){
        $validator = Validator::make($request->all(), [
            'aula' => 'required|numeric|digits_between:1,3',
            'descripcion' => 'required|max:255',
            'ordenador' => 'required|regex:/^HZ[0-9]{6}/'
        ]);
    if ($validator->fails()) {
        return redirect('formIncidencias')
                    ->withErrors($validator)
                    ->withInput();
    }
    else{
        DB::table('incidencias')
        ->where('numero', $numero)  
        ->limit(1) 
        ->update([
            'ordenador' => $request->ordenador,
            'aula' => $request->aula,
            'descripcion' => $request->descripcion,
            'comentario' => $request->comentario,
        ]); 
        return redirect('/enviarcorreo2');


    }
}

}
