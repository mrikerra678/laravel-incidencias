<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidencias', function (Blueprint $table) {
            $table->bigIncrements('numero');
            $table->string('aula');
            $table->string('codigo');
            $table->string('descripcion');
            $table->string('ordenador');
            $table->string('comentario')->default("");
            $table->unsignedInteger('profesor_id');
            $table->string('estado')->default("En Espera");
            $table->foreign('profesor_id')->references('id')->on('profesor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidencias');
    }
}
